﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace lr5
{
    class Graph
    {
        static int COUNT = 12;
        public static int MaxCount
        {
            get { return COUNT; }
        }
        List<Node> nodes = new List<Node>();
        Connection[,] connections = new Connection[COUNT, COUNT];
        Node focusedNode = null;
        Panel canvas;
        Form1 f;

        public int Count
        {
            get { return nodes.Count; }
        }

        public Graph(Panel _canvas, Form1 _f)
        {
            canvas = _canvas;
            f = _f;
            for (int i = 0; i < COUNT; i++)
                for (int j = 0; j < COUNT; j++)
                    connections[i, j] = null;
        }

        public void drawNode(int x, int y)
        {
            resetHighlights();
            if (focusedNode != null) focusedNode.highlight(true);
            if (nodes.Count == COUNT) return;
            Node hitCircle = circleAt(x, y);
            if (hitCircle != null) return;

            Node n = new Node(x, y, (nodes.Count + 1).ToString(), canvas);
            nodes.Add(n);
            n.draw();
            drawTable();
        }

        private Node circleAt(int x, int y)
        {
            for (int i = 0; i < nodes.Count; i++) if (nodes[i].hittest(x, y)) return nodes[i];
            return null;
        }

        public void focusNode(int x, int y)
        {
            resetHighlights();
            if (focusedNode != null) focusedNode.highlight(true);
            Node hitNode = circleAt(x, y);

            if (hitNode == focusedNode)
            {
                if (focusedNode != null) focusedNode.highlight(false);
                focusedNode = null;
                return;
            };

            if (focusedNode == null && hitNode != null)
            {
                focusedNode = hitNode;
                hitNode.highlight(true);
            }
            else if (focusedNode != null && hitNode != null)
            {
                Connection c = new Connection(hitNode, focusedNode, canvas);
                int hitNodeIndex = nodes.FindIndex((Node n) => { return n.ID == hitNode.ID; });
                int focusedNodeIndex = nodes.FindIndex((Node n) => { return n.ID == focusedNode.ID; });
                if (connections[hitNodeIndex, focusedNodeIndex] == null)
                {
                    connections[hitNodeIndex, focusedNodeIndex] = c;
                    connections[focusedNodeIndex, hitNodeIndex] = c;
                }

                drawTable();

                focusedNode.highlight(false);
                focusedNode = null;
            }
        }

        public void drawTable()
        {
            f.tableLayoutPanel2.Controls.Clear();
            for (int i = 0; i < nodes.Count; i++)
            {
                Label l1 = new Label();
                Label l2 = new Label();

                l1.Text = nodes[i].Label;
                l2.Text = nodes[i].Label;

                l1.TextAlign = ContentAlignment.MiddleCenter;
                l2.TextAlign = ContentAlignment.MiddleCenter;

                f.tableLayoutPanel2.Controls.Add(l1, i + 1, 0);
                f.tableLayoutPanel2.Controls.Add(l2, 0, i + 1);
            }

            for (int i = 0; i < nodes.Count; i++)
                for (int j = 0; j < nodes.Count; j++)
                {
                    if (connections[i, j] == null) continue;
                    Label t = new Label();
                    t.BackColor = Color.White;
                    t.TextAlign = ContentAlignment.MiddleCenter;
                    t.Text = connections[i, j].value.ToString();
                    f.tableLayoutPanel2.Controls.Add(t, j + 1, i + 1);
                }
        }

        public void clear()
        {
            f.tableLayoutPanel2.Controls.Clear();
            canvas.CreateGraphics().Clear(Color.White);
            nodes.Clear();

            for (int i = 0; i < COUNT; i++)
                for (int j = 0; j < COUNT; j++)
                    connections[i, j] = null;

        }

        private void resetHighlights()
        {
            for (int i = 0; i < nodes.Count; i++) nodes[i].highlight(false);
        }

        public List<Node> bfs(int startNode = 1)
        {
            if (startNode - 1 >= nodes.Count || nodes.Count == 0) return new List<Node>();

            Queue<Node> q = new Queue<Node>();
            List<Node> processed = new List<Node>();
            q.Enqueue(nodes[startNode - 1]);

            while (q.Count != 0)            
            {
                Node removedNode = q.Dequeue();             
                bool isProcessed = processed.FindIndex((Node n) => n.Label == removedNode.Label) != -1; 
                if (!isProcessed)
                {
                    processed.Add(removedNode);             
                }

                int removedNodeIndex = nodes.FindIndex((Node n) => n.Label == removedNode.Label);   

                for (int i = 0; i < COUNT; i++)     
                {
                    if (connections[removedNodeIndex, i] == null) continue;
                    Node newNode = nodes[i];
                    isProcessed = processed.FindIndex((Node n) => n.Label == newNode.Label) != -1;  
                    if (isProcessed) continue;
                    q.Enqueue(newNode);                                         
                }
            }

            return processed;
        }

        public string searchEularianCycle()
        {
            if (nodes.Count == 0) return "Draw the graph";
            if (!isGraphConnected()) return "Graph does not contain Eularian cycle because it isn not connected";       //?
            if (!areAllNodesEven()) return "Graph does not contain Eularian cycle because there are odd nodes";         //?

            Stack<Node> cycle = getEularianCycle();
            string sequence = "";

            while (cycle.Count != 0) sequence += cycle.Pop().Label + ", ";

            return sequence;
        }

        private bool isGraphConnected()
        {
            int nodesCount = nodes.Count;
            int bfsResultCount = bfs().Count;

            return nodesCount == bfsResultCount;
        }

        private bool areAllNodesEven()
        {
            for(int i = 0; i < nodes.Count; i++)
            {
                if (getConnectionsCount(nodes[i]) % 2 == 1) return false;
            }

            return true;
        }

        private Stack<Node> getEularianCycle()  
        {
            Stack<Node> processingNodes = new Stack<Node>();
            Stack<Node> result = new Stack<Node>();
            Connection[,] connectionsCopy = getConnectionsCopy();
            
            
            Node startNode = nodes[0];
            processingNodes.Push(startNode);        // O(1)

            while (processingNodes.Count != 0)      // O(n^2)     n - nodes count, m - connections count
            {
                Node processingNode = processingNodes.Peek();       // O(1)
                int nodeIndex = getNodeIndex(processingNode);       // O(n)

                if (getConnectionsCount(processingNode, connectionsCopy) != 0)      // O(n) - dominant
                {
                    Node neighbour = null;
                    int neighbourIndex = -1;
                    for (int i = 0; neighbour == null; i++)
                    {                   // O(n)
                        if (connectionsCopy[nodeIndex, i] != null)
                        {
                            neighbour = nodes[i];
                            neighbourIndex = i;
                        }
                    }

                    processingNodes.Push(neighbour);                // O(1)
                    connectionsCopy[nodeIndex, neighbourIndex] = null;
                    connectionsCopy[neighbourIndex, nodeIndex] = null;

                    
                    processingNode = neighbour;
                } else
                {
                    result.Push(processingNodes.Pop());
                }
            }

            return result;
        }

        private int getConnectionsCount(Node n, Connection[,] _connections = null)
        {
            if (_connections == null) _connections = connections;
            int connectionsCount = 0;
            int index = getNodeIndex(n);

            for (int i = 0; i < nodes.Count; i++)
                if (_connections[index, i] != null) connectionsCount++;

            return connectionsCount;
        }

        private int getNodeIndex(Node n)
        {
            return nodes.FindIndex((Node node) => node == n);
        }

        private Connection[,] getConnectionsCopy()
        {
            Connection[,] connectionsCopy = new Connection[COUNT, COUNT];

            for (int i = 0; i < COUNT; i++)
                for (int j = 0; j < COUNT; j++)
                    connectionsCopy[i, j] = connections[i, j];

            return connectionsCopy;
        }
    }


}
