﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lr5
{
    public partial class Form1 : Form
    {
        Graph graph;
        public Form1()
        {
            InitializeComponent();
            graph = new Graph(canvas, this);
        }

        private void canvas_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = e as MouseEventArgs;
            if (me.Button == MouseButtons.Right)
            {
                graph.focusNode(me.X, me.Y);
            }
            else
            {
                graph.drawNode(me.X, me.Y);
            }
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            graph.clear();
        }

        private void SearchCycleBtn_Click(object sender, EventArgs e)
        {
            CycleLabel.Text = graph.searchEularianCycle();
        }
    }
}
