﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace lr5
{
    class Node
    {
        int x;
        int y;
        int width = 40;
        int height = 40;
        string label;
        int id;
        Panel canvas;

        public Node(int _x, int _y, string _label, Panel _canvas)
        {
            x = _x;
            y = _y;
            label = _label;
            canvas = _canvas;
            id = (new Random()).Next();
        }

        public string Label
        {
            get { return label; }
            set { label = value; }
        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        public void draw()
        {
            Graphics g = canvas.CreateGraphics();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            Pen pen = new Pen(Color.Black);
            pen.Width = 2;
            SolidBrush brush = new SolidBrush(Color.White);

            g.FillEllipse(brush, new Rectangle(x - width / 2, y - height / 2, width, height));
            g.DrawEllipse(pen, new Rectangle(x - width / 2, y - height / 2, width, height));

            Font font = new Font("Arial", 16);
            StringFormat format = new StringFormat();
            brush.Color = Color.Black;
            g.DrawString(label, font, brush, x - 10, y - 10);

            pen.Dispose();
            brush.Dispose();
            g.Dispose();
        }

        public bool hittest(int clickX, int clickY)
        {
            return clickX >= x - width / 2 &&
                clickX <= x + width / 2 &&
                clickY >= y - height / 2 &&
                clickY <= y + height / 2;
        }

        public void highlight(bool turnOn)
        {
            Graphics g = canvas.CreateGraphics();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;

            Pen pen = new Pen(turnOn ? Color.Red : Color.Black);
            pen.Width = 2;
            g.DrawEllipse(pen, new Rectangle(x - width / 2, y - height / 2, width, height));

            g.Dispose();
            pen.Dispose();
        }
    }
}
